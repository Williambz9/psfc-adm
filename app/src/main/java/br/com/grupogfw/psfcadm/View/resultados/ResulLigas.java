package br.com.grupogfw.psfcadm.View.resultados;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import br.com.grupogfw.psfcadm.Model.AddJogoModel;
import br.com.grupogfw.psfcadm.Model.ResulJogos;
import br.com.grupogfw.psfcadm.R;

public class ResulLigas extends AppCompatActivity implements View.OnClickListener {

    TextView casa, fora, ccasa, cfora, hora, classico, finl, carre, campe;
    EditText edtcasa, edtfora;
    Button enviar;
    ProgressBar progressBar;
    CardView cardView;


    FirebaseDatabase database;
    DatabaseReference jogos, palpenviados, dias;
    int idatual = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.a_resul_ligas);


        database = FirebaseDatabase.getInstance();
        jogos = database.getReference("Palpites");
        palpenviados = database.getReference("Resultados");
        dias = database.getReference("global");

        casa = (TextView) findViewById(R.id.casa);
        fora = (TextView) findViewById(R.id.fora);
        hora = (TextView) findViewById(R.id.hora);
        classico = (TextView) findViewById(R.id.classic);
        finl = (TextView) findViewById(R.id.fina);
        ccasa = (TextView) findViewById(R.id.casacompleto);
        cfora = (TextView) findViewById(R.id.foracompleto);
        edtcasa = (EditText) findViewById(R.id.palpcasa);
        edtfora = (EditText) findViewById(R.id.palpfora);
        enviar = (Button) findViewById(R.id.palpitar);
        campe = (TextView) findViewById(R.id.campeonato);
        progressBar = (ProgressBar) findViewById(R.id.progress);
        carre = (TextView) findViewById(R.id.carre);
        cardView = (CardView) findViewById(R.id.card);

        enviar.setOnClickListener(this);
        dados();
    }


    public void dados() {

        dias.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                final String idx = String.valueOf(dataSnapshot.child("qtdJogos").getValue());
                final Double idmax = Double.parseDouble(idx);

                jogos.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        String val = "Jogo" + String.valueOf(idatual);

                        if (idatual < idmax) {

                            AddJogoModel model = (dataSnapshot.child(val).getValue(AddJogoModel.class));

                            hora.setText((model.getHora()));
                            fora.setText(String.valueOf(model.getFora().substring(0, 3)));
                            casa.setText(String.valueOf(model.getCasa().substring(0, 3)));
                            cfora.setText(model.getFora());
                            ccasa.setText(model.getCasa());

                            campe.setText(model.getCampeonato());

                            cardView.setVisibility(View.VISIBLE);
                            progressBar.setVisibility(View.GONE);
                            carre.setVisibility(View.GONE);


                        } else if (idatual == idmax) {

                            AddJogoModel model = (dataSnapshot.child(val).getValue(AddJogoModel.class));

                            hora.setText((model.getHora()));
                            fora.setText(String.valueOf(model.getFora().substring(0, 3)));
                            casa.setText(String.valueOf(model.getCasa().substring(0, 3)));
                            cfora.setText(model.getFora());
                            ccasa.setText(model.getCasa());
                            campe.setText(model.getCampeonato());



                            enviar.setText("Concluir");
                            cardView.setVisibility(View.VISIBLE);
                            progressBar.setVisibility(View.GONE);
                            carre.setVisibility(View.GONE);

                        } else {
                            carre.setText("OS resultados foram enviados...");
                            progressBar.setVisibility(View.GONE);
                        }


                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


    }

    @Override
    public void onClick(View view) {
        String valCasa = edtcasa.getText().toString();
        String valFora = edtfora.getText().toString();
        Double c = Double.parseDouble(valCasa);
        Double f = Double.parseDouble(valFora);
        String fi = finl.getText().toString();
        String cl = classico.getText().toString();
        int pts;
        if (fi.equals(".") && cl.equals(".")) {
            pts = 12;
        } else if (fi.equals(".") || cl.equals(".")) {
            pts = 6;
        } else {
            pts = 3;
        }


        String jogo = null;
        if (c > f) {
            jogo = "casa";
        }
        if (c.equals(f)) {
            jogo = "empate";
        }
        if (c < f) {
            jogo = "fora";
        }

        ResulJogos plt = new ResulJogos(valCasa, valFora, jogo, pts);
        palpenviados.child("Liga").child("Jogo" + String.valueOf(idatual)).setValue(plt);

        idatual = idatual + 1;
        dados();
        edtfora.setText("");
        edtcasa.setText("");
        finl.setText("");
        classico.setText("");
        finl.setVisibility(View.GONE);
        classico.setVisibility(View.GONE);
        cardView.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);
        carre.setVisibility(View.VISIBLE);


    }
}
