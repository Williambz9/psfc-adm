package br.com.grupogfw.psfcadm.View.classificacao;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.StorageReference;

import java.text.DecimalFormat;

import br.com.grupogfw.psfcadm.Model.AddJogoModel;
import br.com.grupogfw.psfcadm.Model.Ligaposi;
import br.com.grupogfw.psfcadm.Model.ResulJogos;
import br.com.grupogfw.psfcadm.Model.ResulJogosplayes;
import br.com.grupogfw.psfcadm.Model.Usuario;
import br.com.grupogfw.psfcadm.R;
import br.com.grupogfw.psfcadm.View.addjogo.AddJogos;

public class pontosRodadas extends AppCompatActivity {


    FirebaseDatabase database;
    DatabaseReference usuarios, palpEnviados, ligas, resultados, times, global;
    int id = 1;
    int idjogo = 1;
    TextView textView;
    ProgressBar progressBar;
    Button b;
    Usuario usuario;
    String next = "teta";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.a_pontos_rodadas);
        progressBar = (ProgressBar) findViewById(R.id.progress);

        database = FirebaseDatabase.getInstance();
        usuarios = database.getReference("usuarios");
        palpEnviados = database.getReference("palpEnviados");
        ligas = database.getReference("Ligas");
        resultados = database.getReference("Resultados");
        times = database.getReference("Times");
        global = database.getReference("global");
        textView = (TextView) findViewById(R.id.texto);
        textView.setText("Usuario " + id + "," + idjogo + " jogos verificados");
        b = (Button) findViewById(R.id.posi);
        b.setText("Limpar palpites recebidos");

        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                palpEnviados.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if (dataSnapshot.exists()) {
                            progressBar.setVisibility(View.VISIBLE);
                            textView.setText("Removendo...");
                            palpEnviados.removeValue();
                            finish();

                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

            }
        });


        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Deseja mesmo conferir os resultados?");

        builder.setPositiveButton("Sim", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface arg0, int arg1) {

                result();


            }
        });
        builder.setNegativeButton("Não", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                finish();
            }
        });
        builder.show();


    }


    public void result() {


        times.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                //PEGANDO O NOME PELO ID NA TABELA TIMES
                //SE O ID ATUAL EXISTIR ELE CONTINUA, SE NÃO codi();
                //CRIAR VARIAVEL NOME USUARIO
                //CARREGAR DADOS DO USUARIO

                if (dataSnapshot.child(String.valueOf(id)).exists()) {
                    Log.d("tag", "Pegando o usuario");
                    final String nome = String.valueOf(dataSnapshot.child(String.valueOf(id)).child("nome").getValue());


                    usuarios.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            //SE EXISTE CARREGAR DADOS DO USUARIO
                            //SE NÃO CODI();

                            if (dataSnapshot.child(nome).exists()) {
                                //USUARIO EXISTE
                                Log.d("tag", "Carregando o usuario");
                                usuario = dataSnapshot.child(nome).getValue(Usuario.class);


                                palpEnviados.addListenerForSingleValueEvent(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(DataSnapshot dataSnapshot) {

                                        //CARREGAR PALPITES
                                        //SE O PALPITE EXISTI CARREGAR, SE NÃO, CODI();

                                        Log.d("tag", "Carregando palpites dos usuarios");
                                        if (!dataSnapshot.child(usuario.getLiga()).child(usuario.getNomedotime()).exists()) {
                                            //USUARIO NÃO PALPITOU
                                            id = id + 1;
                                            restart();
                                        } else if (dataSnapshot.child(usuario.getLiga()).child(usuario.getNomedotime()).child("Jogo" + idjogo).exists()) {
                                            //USUARIO PALPITOU
                                            final ResulJogosplayes resul = dataSnapshot.child(usuario.getLiga()).child(usuario.getNomedotime()).child("Jogo" + idjogo).getValue(ResulJogosplayes.class);

                                            resultados.addListenerForSingleValueEvent(new ValueEventListener() {
                                                @Override
                                                public void onDataChange(DataSnapshot dataSnapshot) {

                                                    //CARREGAR OS RESULTADOS
                                                    //E PONTUAÇÃO

                                                    Log.d("tag", "Carregando resultados");
                                                    ResulJogos resulJogos = dataSnapshot.child("Liga").child("Jogo" + idjogo).getValue(ResulJogos.class);
                                                    int pts = resulJogos.getPts();

                                                    if (resul.getCasa().equals(resulJogos.getCasa()) && resul.getFora().equals(resulJogos.getFora())) {
                                                        pts = pts;
                                                    } else if (resul.getJogo().equals(resulJogos.getJogo())) {
                                                        pts = pts / 3;
                                                    } else {
                                                        pts = 0;
                                                    }

                                                    final int finalPts = pts;
                                                    ligas.addListenerForSingleValueEvent(new ValueEventListener() {
                                                        @Override
                                                        public void onDataChange(DataSnapshot dataSnapshot) {
                                                            //ADICIONAR NA LIGA

                                                            int ptrod;
                                                            Log.d("tag", "Carregando usuario/liga");
                                                            Ligaposi ligaposi = dataSnapshot.child(usuario.getLiga()).child(usuario.getNomedotime()).getValue(Ligaposi.class);

                                                            if (idjogo == 1) {
                                                                ptrod = 0;
                                                            } else {
                                                                ptrod = ligaposi.getRodada();
                                                            }

                                                            int ptons = ligaposi.getPontos();
                                                            ptons = ptons + finalPts;
                                                            ptrod = ptrod + finalPts;
                                                            String ptserodada = String.valueOf(ptons) + "." + String.valueOf(ptrod);
                                                            Double ptrs = Double.parseDouble(ptserodada);
                                                            Ligaposi liga = new Ligaposi(ligaposi.getTime(), ptons, ptrod, ptrs);
                                                            ligas.child(usuario.getLiga()).child(usuario.getNomedotime()).setValue(liga);
                                                            codi();

                                                        }

                                                        @Override
                                                        public void onCancelled(DatabaseError databaseError) {
                                                            Toast.makeText(pontosRodadas.this, "Ocorreu um erro", Toast.LENGTH_LONG).show();

                                                        }
                                                    });


                                                }


                                                @Override
                                                public void onCancelled(DatabaseError databaseError) {
                                                    Toast.makeText(pontosRodadas.this, "Ocorreu um erro", Toast.LENGTH_LONG).show();

                                                }
                                            });


                                        } else {

                                            //PALPITE NÃO EXISTE

                                            codi();


                                        }


                                    }

                                    @Override
                                    public void onCancelled(DatabaseError databaseError) {
                                        Toast.makeText(pontosRodadas.this, "Ocorreu um erro", Toast.LENGTH_LONG).show();

                                    }
                                });


                            } else {
                                //USUARIO NÃO EXISTE
                                Log.d("T", "User nao existe");

                                next = "nex";
                                codi();


                            }


                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                            Toast.makeText(pontosRodadas.this, "Ocorreu um erro", Toast.LENGTH_LONG).show();

                        }
                    });


                } else {
                    codi();
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Toast.makeText(pontosRodadas.this, "Ocorreu um erro", Toast.LENGTH_LONG).show();

            }
        });


    }

    public void restart() {
        result();
    }

    public void codi() {
        global.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                final String qtd = String.valueOf(dataSnapshot.child("qtdJogos").getValue());
                final int quantJogos = Integer.parseInt(qtd);

                Log.d("tag", "Reiniciando as paradas");
                Log.d("tag", next);
                global.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        String iduse = String.valueOf(dataSnapshot.child("UserId").getValue());
                        int idusuario = Integer.parseInt(iduse);


                        if (next.equals("nex") && idusuario == id) {
                            Log.d("tag", "id = id");
                            textView.setText("Encerrou...");
                            Log.d("tag", "Quantidade de jogos é igual a quantidade de jogos e usuarios corrigidos");
                            Log.d("estado", "Fim");
                          progressBar.setVisibility(View.GONE);


                        } else if (next.equals("nex") && idusuario > id) {
                            Log.d("tag", "id > id");
                            id = id + 1;
                            idjogo = 1;
                            Log.d("tag", "Quantidade de jogos é igual a quantidade de jogos corrigidos");
                            textView.setText("Proximo usuario...");
                            restart();


                        } else if (quantJogos == idjogo && idusuario == id) {


                            textView.setText("Encerrou...");
                            Log.d("tag", "Quantidade de jogos é igual a quantidade de jogos e usuarios corrigidos");
                            Log.d("estado", "Fim");
                            progressBar.setVisibility(View.GONE);
                        } else if (quantJogos == idjogo) {
                            id = id + 1;
                            idjogo = 1;
                            Log.d("tag", "Quantidade de jogos é igual a quantidade de jogos corrigidos");
                            textView.setText("Proximo usuario...");
                            restart();
                        } else if (quantJogos > idjogo) {
                            Log.d("tag", "Quantidade de jogos é diferente a quantidade de jogos corrigidos");

                            idjogo = idjogo + 1;
                            textView.setText(usuario.getNomedotime() + " ," + idjogo + " de " + qtd + " jogos");
                            restart();
                        }

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


    }
}
