package br.com.grupogfw.psfcadm.Model;

/**
 * Created by william on 31/01/18.
 */

public class ResulJogosplayes {

    String casa;
    String fora;
    String jogo;

    public ResulJogosplayes() {
    }

    public ResulJogosplayes(String casa, String fora, String jogo) {
        this.casa = casa;
        this.fora = fora;
        this.jogo = jogo;
    }

    public String getFora() {
        return fora;
    }

    public String getJogo() {
        return jogo;
    }

    public void setJogo(String jogo) {
        this.jogo = jogo;
    }

    public String getCasa() {
        return casa;
    }

    public void setCasa(String casa) {
        this.casa = casa;
    }

    public void setFora(String fora) {
        this.fora = fora;
    }
}
