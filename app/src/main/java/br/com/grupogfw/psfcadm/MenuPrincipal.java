package br.com.grupogfw.psfcadm;

import android.app.AlertDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import br.com.grupogfw.psfcadm.View.Classificacoes.Tabela;
import br.com.grupogfw.psfcadm.View.addjogo.AddJogos;
import br.com.grupogfw.psfcadm.View.classificacao.pontosRodadas;
import br.com.grupogfw.psfcadm.View.resultados.MenuResultados;

public class MenuPrincipal extends AppCompatActivity {

    Button add, resul, publ, clasi;
    FirebaseDatabase database;
    DatabaseReference jogos, dias, resultados;
    TextView temp, rodada;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_principal);

        database = FirebaseDatabase.getInstance();
        jogos = database.getReference("Palpites");
        dias = database.getReference("global");
        resultados = database.getReference("Resultados");

        temp = (TextView) findViewById(R.id.temp);
        rodada = (TextView) findViewById(R.id.rod);

        dias.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                temp.setText("Temporada: " + dataSnapshot.child("Temp").getValue());
                rodada.setText("Rodada: " + dataSnapshot.child("Rodada").getValue());

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        final DatabaseReference palpEnviados = database.getReference("palpEnviados");



        Button del = (Button) findViewById(R.id.deletar);
        del.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                palpEnviados.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if (dataSnapshot.exists()) {

                            AlertDialog.Builder builder = new AlertDialog.Builder(MenuPrincipal .this);
                            builder.setTitle("Espere....");
                            builder.setMessage("Pode demoras um pouco");
                            builder.show();
                            palpEnviados.removeValue();
                            finish();



                        }
                        else {
                            AlertDialog.Builder builder = new AlertDialog.Builder(MenuPrincipal .this);
                            builder.setTitle("Ops,");
                            builder.setMessage("Os dados ja foram apagados!");
                            builder.show();

                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });


            }
        });
        Button del2 = (Button) findViewById(R.id.deletar1);
        del2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                jogos.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if (dataSnapshot.exists()) {

                            AlertDialog.Builder builder = new AlertDialog.Builder(MenuPrincipal .this);
                            builder.setTitle("Espere....");
                            builder.setMessage("Pode demorar um pouco");
                            builder.show();
                            jogos.removeValue();
                            finish();



                        }
                        else {
                            AlertDialog.Builder builder = new AlertDialog.Builder(MenuPrincipal .this);
                            builder.setTitle("Ops,");
                            builder.setMessage("Os dados ja foram apagados!");
                            builder.show();

                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });


            }
        });
        Button del3 = (Button) findViewById(R.id.deletar2);
        del3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resultados.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if (dataSnapshot.exists()) {

                            AlertDialog.Builder builder = new AlertDialog.Builder(MenuPrincipal .this);
                            builder.setTitle("Espere....");
                            builder.setMessage("Pode demorar um pouco");
                            builder.show();
                            resultados.removeValue();
                            finish();



                        }
                        else {
                            AlertDialog.Builder builder = new AlertDialog.Builder(MenuPrincipal .this);
                            builder.setTitle("Ops,");
                            builder.setMessage("Os dados ja foram apagados!");
                            builder.show();

                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });


            }
        });



        add = (Button) findViewById(R.id.add);
        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent t = new Intent(MenuPrincipal.this, AddJogos.class );
                startActivity(t);
            }
        });
        clasi = (Button) findViewById(R.id.ligas);
        clasi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent t = new Intent(MenuPrincipal.this, Tabela.class );
                t.putExtra("Serie", "A");
                startActivity(t);
            }
        });
        resul = (Button) findViewById(R.id.resultados);
        resul.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MenuPrincipal.this, MenuResultados.class));
            }
        });
        publ = (Button) findViewById(R.id.conferir);
        publ.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MenuPrincipal.this, pontosRodadas.class));
            }
        });

    }
}
