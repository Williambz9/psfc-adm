package br.com.grupogfw.psfcadm.View.addjogo;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.DecimalFormat;

import br.com.grupogfw.psfcadm.Model.AddJogoModel;
import br.com.grupogfw.psfcadm.R;

public class AddJogos extends AppCompatActivity implements View.OnClickListener {

    AutoCompleteTextView casa, fora, hora, min, campeonato;
    CheckBox grup, oitavas, quartas, semi, finam, terceiro;
    Button proximo, concluir;
    TextView mens, texto;
    ProgressBar progressBar;

    FirebaseDatabase database;
    DatabaseReference jogos, dias;
    LinearLayout linearLayout;

    int jogo = 1;
    int idjogo = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_jogos);

        casa = (AutoCompleteTextView) findViewById(R.id.casa);
        fora = (AutoCompleteTextView) findViewById(R.id.fora);
        hora = (AutoCompleteTextView) findViewById(R.id.hora);
        min = (AutoCompleteTextView) findViewById(R.id.min);
        campeonato = (AutoCompleteTextView) findViewById(R.id.campeonato);
        linearLayout = (LinearLayout) findViewById(R.id.fundo);
        texto = (TextView) findViewById(R.id.texto);

        texto.setText("Jogo" + jogo);

        progressBar = (ProgressBar) findViewById(R.id.progress);
        mens = (TextView) findViewById(R.id.text);

        grup = (CheckBox) findViewById(R.id.grupos);
        oitavas = (CheckBox) findViewById(R.id.oitavas);
        quartas = (CheckBox) findViewById(R.id.quartas);
        semi = (CheckBox) findViewById(R.id.semi);
        terceiro = (CheckBox) findViewById(R.id.terceiro);
        finam = (CheckBox) findViewById(R.id.finaa);


        proximo = (Button) findViewById(R.id.confirm);
        concluir = (Button) findViewById(R.id.concluir);
        database = FirebaseDatabase.getInstance();
        jogos = database.getReference("Palpites");
        dias = database.getReference("global");

        proximo.setOnClickListener(this);

        concluir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dias.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {

                        Double roda = Double.parseDouble(String.valueOf(dataSnapshot.child("Rodada").getValue()));
                        AlertDialog.Builder builder = new AlertDialog.Builder(AddJogos.this);
                        builder.setTitle("Confirme");
                        final Double r = roda + 1;
                        DecimalFormat formato = new DecimalFormat("#");
                        final Double ra = Double.valueOf(formato.format(r));

                        builder.setMessage("Quantidade de Jogos: " + jogo + "\nRodada: " + ra);
                        builder.setPositiveButton("Confirmar", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface arg0, int arg1) {


                                String fase = null;
                                if (grup.isChecked()) {
                                    fase = "grup";
                                } else if (oitavas.isChecked()) {
                                    fase = "oita";
                                } else if (quartas.isChecked()) {
                                    fase = "quar";
                                } else if (semi.isChecked()) {
                                    fase = "semi";
                                } else if (terceiro.isChecked()) {
                                    fase = "terc";
                                } else if (finam.isChecked()) {
                                    fase = "fina";
                                }


                                AddJogoModel add = new AddJogoModel(casa.getText().toString(),
                                        fora.getText().toString(),
                                        hora.getText().toString() + "h" + min.getText().toString() + "min",
                                        fase,
                                        campeonato.getText().toString());


                                jogos.child("Jogo" + jogo).setValue(add);


                                dias.child("Rodada").setValue(ra);
                                dias.child("qtdJogos").setValue(jogo);
                                finish();


                            }
                        });
                        builder.setNegativeButton("cancelar", null);
                        builder.show();
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });


            }
        });

    }

    @Override
    public void onClick(View view) {

        linearLayout.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);
        mens.setVisibility(View.VISIBLE);


        String fase = null;
        if (grup.isChecked()) {
            fase = "grup";
        } else if (oitavas.isChecked()) {
            fase = "oita";
        } else if (quartas.isChecked()) {
            fase = "quar";
        } else if (semi.isChecked()) {
            fase = "semi";
        } else if (terceiro.isChecked()) {
            fase = "terc";
        } else if (finam.isChecked()) {
            fase = "fina";
        }


        AddJogoModel add = new AddJogoModel(casa.getText().toString(),
                fora.getText().toString(),
                hora.getText().toString() + "h" + min.getText().toString() + "min",
                fase,
                campeonato.getText().toString());

        jogos.child("Jogo" + jogo).setValue(add);

        casa.setText("");
        fora.setText("");
        hora.setText("");
        min.setText("");
        campeonato.setText("");


        linearLayout.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.GONE);
        mens.setVisibility(View.GONE);
        jogo = jogo + 1;
        texto.setText("Jogo " + jogo);
        idjogo = idjogo + 1;


    }
}
