package br.com.grupogfw.psfcadm.View.Classificacoes;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import br.com.grupogfw.psfcadm.Control.TabelaViewHolder;
import br.com.grupogfw.psfcadm.Model.ComptRanking;
import br.com.grupogfw.psfcadm.R;

public class Tabela extends AppCompatActivity {

    FirebaseDatabase database;
    DatabaseReference competicoes;
    FirebaseRecyclerAdapter<ComptRanking, TabelaViewHolder> adapter;
    RecyclerView recyclerView;
    LinearLayoutManager layoutManager;
    ProgressBar progressBar;
    TextView textView, erro;
    LinearLayout top;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tabela);
        textView = (TextView) findViewById(R.id.carre);
        erro = (TextView) findViewById(R.id.conexao);
        top = (LinearLayout) findViewById(R.id.lay);
        progressBar = (ProgressBar) findViewById(R.id.progress);

        Intent intent = getIntent();
        Bundle dados = intent.getExtras();
        getSupportActionBar().setElevation(0);

        String serie = dados.getString("Serie");



        recyclerView = (RecyclerView) findViewById(R.id.tabela);
        database = FirebaseDatabase.getInstance();
        competicoes = database.getReference("Ligas/Série " + serie);


        competicoes.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {

                    textView.setVisibility(View.GONE);
                    recyclerView.setVisibility(View.VISIBLE);
                    progressBar.setVisibility(View.GONE);
                    top.setVisibility(View.VISIBLE);

                } else {
                    textView.setVisibility(View.GONE);
                    progressBar.setVisibility(View.GONE);
                    erro.setVisibility(View.VISIBLE);
                    erro.setText("Essa a liga não possui dados para serem apresentados...");
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                textView.setVisibility(View.GONE);
                progressBar.setVisibility(View.GONE);
                erro.setVisibility(View.VISIBLE);
            }
        });


        layoutManager = new LinearLayoutManager(this);
        recyclerView.setHasFixedSize(true);
        layoutManager.setReverseLayout(true);

        recyclerView.setLayoutManager(layoutManager);


        getSupportActionBar().setTitle("Série " + serie);


        adapter = new FirebaseRecyclerAdapter<ComptRanking, TabelaViewHolder>(
                ComptRanking.class,
                R.layout.itemtabela,
                TabelaViewHolder.class,
                competicoes.orderByChild("ptsrodada")
        ) {

            @Override
            protected void populateViewHolder(TabelaViewHolder viewHolder, ComptRanking model, int position) {


                viewHolder.time.setText(String.valueOf(model.getTime()));
                viewHolder.pontos.setText(String.valueOf(model.getPontos()));
                viewHolder.rodada.setText(String.valueOf(model.getRodada()));
                viewHolder.posi.setText("");



            }


        };


        adapter.notifyDataSetChanged();
        recyclerView.setAdapter(adapter);


    }
}
