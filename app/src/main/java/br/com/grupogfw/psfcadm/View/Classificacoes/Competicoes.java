package br.com.grupogfw.psfcadm.View.Classificacoes;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import br.com.grupogfw.psfcadm.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class Competicoes extends Fragment {


    public Competicoes() {
        // Required empty public constructor
    }
    CardView lig1, lig2, lig3;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.f_competicoes, container, false);

        lig1 = (CardView) view.findViewById(R.id.serieA);
        lig2 = (CardView) view.findViewById(R.id.serieB);
        lig3 = (CardView) view.findViewById(R.id.serieC);

        lig1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent t = new Intent(getActivity(), Tabela.class );
                t.putExtra("Serie", "A");
                startActivity(t);
            }
        });
        lig2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent t = new Intent(getActivity(), Tabela.class );
                t.putExtra("Serie", "B");
                startActivity(t);
            }
        });    lig3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent t = new Intent(getActivity(), Tabela.class );
                t.putExtra("Serie", "C");
                startActivity(t);
            }
        });





   return view;
    }

}
