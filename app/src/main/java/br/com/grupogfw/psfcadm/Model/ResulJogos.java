package br.com.grupogfw.psfcadm.Model;

/**
 * Created by william on 31/01/18.
 */

public class ResulJogos {

    String casa;
    String fora;
    String jogo;
    int pts;

    public ResulJogos() {
    }

    public ResulJogos(String casa, String fora, String jogo, int pts) {
        this.casa = casa;
        this.fora = fora;
        this.jogo = jogo;
        this.pts = pts;
    }

    public int getPts() {
        return pts;
    }

    public String getFora() {
        return fora;
    }

    public String getJogo() {
        return jogo;
    }

    public void setJogo(String jogo) {
        this.jogo = jogo;
    }

    public String getCasa() {
        return casa;
    }

    public void setCasa(String casa) {
        this.casa = casa;
    }

    public void setFora(String fora) {
        this.fora = fora;
    }
}
