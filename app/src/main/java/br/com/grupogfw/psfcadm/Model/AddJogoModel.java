package br.com.grupogfw.psfcadm.Model;

/**
 * Created by william on 30/01/18.
 */

public class AddJogoModel {
    String casa, fora, hora, fase, campeonato;


    public AddJogoModel() {

    }

    public AddJogoModel(String casa, String fora, String hora, String fase, String campeonato) {
        this.casa = casa;
        this.fora = fora;
        this.hora = hora;
        this.fase = fase;

        this.campeonato = campeonato;

    }

    public String getCampeonato() {
        return campeonato;
    }


    public String getCasa() {
        return casa;
    }

    public String getFora() {
        return fora;
    }

    public String getHora() {
        return hora;
    }
}
