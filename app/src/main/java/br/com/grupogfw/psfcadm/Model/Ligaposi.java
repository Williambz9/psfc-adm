package br.com.grupogfw.psfcadm.Model;

/**
 * Created by william on 06/02/18.
 */

public class Ligaposi {

    String time;
    int  pontos, rodada;
    Double ptsrodada;

    public Ligaposi() {
    }

    public Ligaposi(String time,int pontos, int rodada, Double ptsrodada){
        this.time = time;
        this.pontos = pontos;
        this.rodada = rodada;
        this.ptsrodada = ptsrodada;

    }

    public Double getPtsrodada() {
        return ptsrodada;
    }

    public int getPontos() {
        return pontos;
    }

    public int getRodada() {
        return rodada;
    }

    public String getTime() {
        return time;
    }

}
