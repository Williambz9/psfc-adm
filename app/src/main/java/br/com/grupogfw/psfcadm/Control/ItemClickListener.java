package br.com.grupogfw.psfcadm.Control;

import android.view.View;

/**
 * Created by william on 29/12/17.
 */

public interface ItemClickListener {
    void onClick(View ciew, int position, boolean isLongClick);
}
