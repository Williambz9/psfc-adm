package br.com.grupogfw.psfcadm.Model;

/**
 * Created by william on 30/01/18.
 */

public class Jogo {
    String casa, fora, hora, finl, classico, campeonato;


    public Jogo() {

    }

    public Jogo(String casa, String fora, String hora, String finl, String classico, String campeonato) {
        this.casa = casa;
        this.fora = fora;
        this.hora = hora;
        this.campeonato = campeonato;
        this.finl = finl;
        this.classico = classico;

    }

    public String getCampeonato() {
        return campeonato;
    }

    public String getClassico() {
        return classico;
    }

    public String getFinl() {
        return finl;
    }

    public String getCasa() {
        return casa;
    }

    public String getFora() {
        return fora;
    }

    public String getHora() {
        return hora;
    }
}
